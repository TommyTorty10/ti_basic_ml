# Ti BASIC Machine Learning POC
I wanted to learn about the basics of machine learning, so I built
a bare bones proof of concept program in Ti BASIC on my Ti 83+.  

If you want to do something like this, I highly recommend getting yourself
a programming cable, so you can use an IDE.  I prefer [Cemetech's](https://www.cemetech.net/)
online IDE.

## Program Overview
Since I'm using a Ti-83+, I don't have the ability to use functions.
Every variable is a global variable.  However, I can have a program
execute another program which allows me to break it up a little bit.

#### Variables
The following table lists the variables and their meanings of the programs.
Use of [] around a letter is Ti BASIC notation for a matrix.
|Variable|Meaning|
|-------|-------|
|L      |Learning rate 0<L<1|
|R      |# of rows of [A]|
|C      |# of cols of [A]|
|L1     |input data to make a prediction from|
|L2     |expected/example output to use for training|
|L3     |holds a copy of the output layer for UI purposes|
|L4     |copy of the last row of the error matrix to calculate a single, net error|
|[A]    |Neurons/Values matrix|
|[B]    |weights matrix|
|[E]    |error matrix|
|[D]    |intermediary calculation matrix; derivative of the sigmoid function multiplied by the error matrix|

#### NETWORKS
NETWORKS stands for network setup.  This program prompts the user
for the dimensions of the matrix of values [A] as well as the learning rate,
L. It then declares the other matrices for the weights, error, and derivative
of the sigmoid calculations.

#### PREDICT
This program takes an input given in L1, and attempts to make
a prediction outputted in L3.  I chose to use the sigmoid activation function,
because it was easy to implement at the time.

#### TRAIN
This program makes a prediction, calculates the error of the prediction, and updates
the weights based on the error. I chose to use the derivative of the sigmoid function in
my error calculations.

## Design Reasoning
I chose not to iterate the program through multiple training
cycles, because it is very slow on an actual calculator.  This limits
its usefulness. I have found that interpolation is the only application is actually useful.
The Z80 processor does not have any parallelization capabilities,
so there is likely no way to make this program practical for wider applications.
I have considered rewriting it in assembly to take try and sqeeze
out some more performance, but this project is already rediculous enough.
